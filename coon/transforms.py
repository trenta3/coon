"""
A transform is a class that is given each object with attributes in a given subtree and can output a
transformed version of them, by also keeping state from higher tree level to nested ones.
"""
from coon.components import Component, Module
from coon.utils import deepcopy


__pragma__("kwargs")


class Transform:
    def transform_component(self, tag, inner, attributes, state=None):
        return tag, inner, attributes, state

    def __call__(self, element, state=None):
        """
        Applies the transformation to the given dom_root.
        Returns only the transformed element.
        """
        if isinstance(element, Module):
            element = element.dom_root()
        if isinstance(element, str):
            return element
        if callable(element):
            return lambda *args, **kwargs: self.__call__(element(*args, **kwargs), state=deepcopy(state))
        if not isinstance(element, Component):
            raise Exception("The element to transform must be a Component instance, instead it is of class " +
                            type(element))
        tag, inner, attributes, state = self.transform_component(
            deepcopy(element.tag),
            deepcopy(element.inner),
            deepcopy(element.attributes),
            deepcopy(state),
        )
        if isinstance(inner, list):
            inner = list(map(lambda el: self.__call__(el, state=deepcopy(state)), inner))
        else:
            inner = self.__call__(inner, state=deepcopy(state))
        newelement = Component(inner=inner, **attributes)
        newelement.tag = tag
        return newelement
