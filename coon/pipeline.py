from org.transcrypt.stubs.browser import __pragma__
from coon.utils import deepcopy, pp, partial


__pragma__("kwargs")
__pragma__("opov")


class NoOutput:
    "Class to represent a pipeline element which never returned any output."
    def __str__(self):
        return "<NoOutput>"

    def __repr__(self):
        return "<NoOutput>"


class Pipeline:
    """
    Implements a system of dependent function calls, each of which has an output which can be used by other pipelines.
    """
    caching = True

    def __call__(self, *args, **kwargs):
        "Specify the dependency between the passed pipelines as arguments and the current pipeline output"
        if hasattr(self, "call_args") or hasattr(self, "call_kwargs"):
            raise Exception(f"Pipeline already set up with {self.call_args=} and {self.call_kwargs=}")
        self.call_args = args
        self.call_kwargs = kwargs
        if not hasattr(self, "output_cbs"):
            self.output_cbs = []
        # Subscribe to all input pipelines
        for pipe, elbuilder in list(traverse_dissect((args, kwargs))):
            pipe.subscribe(
                partial(
                    (lambda out, elbuilder: self.input_changed(*elbuilder(out))),
                    elbuilder=elbuilder
                )
            )
        return self

    def subscribe(self, cb):
        if not hasattr(self, "output_cbs"):
            self.output_cbs = []
        if not callable(cb):
            raise Exception("Passed callback is not callable!")
        self.output_cbs.append(cb)

    def input_changed(self, args, kwargs):
        args = traverse_get_output(args)
        kwargs = traverse_get_output(kwargs)
        output = self.fn(*args, **kwargs)
        if output != NoOutput:
            # Notify downstream elements of the change
            self.output_changed(output)

    def output_changed(self, output):
        # Cache the current output if desired
        if self.caching:
            self.cached_output = output
        # Notify the descendants!
        if hasattr(self, "output_cbs"):
            for cb in self.output_cbs:
                cb(output)
        
    def get_output(self):
        "Returns the last computed output, or NoOutput"
        if self.caching and hasattr(self, "cached_output"):
            return self.cached_output
        return NoOutput
    
    def fn(self, *args, **kwargs):
        "The real processing function to override"
        raise NotImplemented


def traverse_get_output(structure):
    "Traverses a structure to convert all Pipeline component into their outputs"
    if structure is None:
        return None
    for T in [str, int, float, bool, bytes]:
        if isinstance(structure, T):
            return structure
    if isinstance(structure, list):
        return list(map(lambda x: traverse_get_output(x), structure))
    if isinstance(structure, tuple):
        return tuple(map(lambda x: traverse_get_output(x), structure))
    if isinstance(structure, dict):
        return {key: traverse_get_output(value) for key, value in structure.items()}
    if isinstance(structure, Pipeline):
        return structure.get_output()
    print("UNK", structure, structure.__class__)
    raise Exception(f"Unknown type of structure: {type(structure)}")


def _tuple(*args):
    return tuple(args)


def _list(*args):
    return list(args)


def traverse_dissect(structure):
    """
    Traverses a structural element to dissect it into each Pipeline component,
    and returns the pipeline component, and a function that given the pipeline
    output returns the original structure with that element substituted!
    Yields such things!
    """
    if structure is None:
        return
    for T in [str, int, float, bool, bytes]:
        if isinstance(structure, T):
            return
    if isinstance(structure, list):
        for i, element in enumerate(structure):
            bfs, afs = structure[:i], structure[i+1:]
            for pipe, elbuilder in list(traverse_dissect(element)):
                builder = partial(
                    (lambda el, elbuilder, bfs, afs: _list(*bfs, elbuilder(el), *afs)),
                    elbuilder=elbuilder, bfs=bfs, afs=afs
                )
                yield pipe, builder
        return
    if isinstance(structure, tuple):
        for i, element in enumerate(structure):
            bfs, afs = structure[:i], structure[i+1:]
            for pipe, elbuilder in list(traverse_dissect(element)):
                builder = partial(
                    (lambda el, elbuilder, bfs, afs: _tuple(*bfs, elbuilder(el), *afs)),
                    elbuilder=elbuilder, bfs=bfs, afs=afs
                )
                yield pipe, builder
        return
    if isinstance(structure, dict):
        for key, value in structure.items():
            for pipe, elbuilder in list(traverse_dissect(value)):
                builder = partial(
                    (lambda el, elbuilder, key: {k: v if k != key else elbuilder(el) for k, v in structure.items()}),
                    elbuilder=elbuilder, key=key
                )
                yield pipe, builder
        return
    if isinstance(structure, Pipeline):
        yield structure, lambda x: x
        return
    raise Exception(f"Unknown structure of type {type(structure)}")


def P(func):
    "Transforms a 'pure' function into a Pipeline element"
    class Pipe(Pipeline):
        def fn(self, *args, **kwargs):
            return func(*args, **kwargs)
    return Pipe()
    
