"""
Define some types that can have constraints and that can be composed to build a data abstraction.
"""

#__pragma__("kwargs")
#__pragma__("opov")


class NoDefault:
    pass


class ConcreteType:
    name = "<ConcreteType>"
    
    def __init__(self, *args, **kwargs):
        "Must be implemented by subclassing types"
        raise NotImplemented

    def validate(self, obj) -> bool:
        "Returns whether obj is a valid instance of the given concrete type"
        return False

    def serialize(self, obj) -> "JSON OBJECT":
        raise NotImplemented

    def unserialize(self, json_obj) -> "Class Instance":
        raise NotImplemented

    
class FunctorialType:
    name = "<FuncType>"
    
    def __init__(self, *args, **kwargs):
        raise Exception("Functorial type " + self.name + " cannot be initialized before subclassing!")

    def __getitem__(self, *args):
        "Must be implemented by subclassing types. Should return a concrete type."
        raise NotImplemented


class Sequence(ConcreteType):
    name = "seq"
    def __init__(self, min_length=None, max_length=None, default=NoDefault, validate=lambda x: True):
        self.min_length = min_length
        self.max_length = max_length
        self.validate = validate
        self.default = default

    def validate(self, obj):
        if self.min_length is not None and len(obj) < min_length:
            return False
        if self.max_length is not None and len(obj) > max_length:
            return False
        return self.validate(obj)
        
            
class String(Sequence):
    name = "str"
    def validate(self, obj):
        return isinstance(obj, str) and super().validate(obj)

    def serialize(self, obj):
        return obj

    def unserialize(self, json):
        return json


class List(FunctorialType):
    name = "List"

    def __getitem__(self, item_type):
        if not isinstance(item_type, ConcreteType):
            raise Exception("List can be composed only over Concrete Types!")
        class ConcreteList(Sequence):
            name = f"List[{item_type.name}]"
            def validate(self, obj):
                if not isinstance(obj, list):
                    return False
                for element in obj:
                    if not item_type.validate(element):
                        return False
                return super().validate(obj):

            def serialize(self, obj):
                return [item_type.serialize(el) for el in obj]

            def unserialize(self, obj):
                return [item_type.unserialize(el) for el in obj]
        return ConcreteList


def dataclass(cls):
    fieldtypes = {field: getattr(cls, field) for field in dir(cls) if not field.startswith("_")}
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key not in fieldtypes.keys():
                raise Exception("Key " + key + " is not in class fields: " + fields)
            if not fieldtypes[key].validate(value):
                raise Exception("Field " + key + " of value " + value + " does not pass validation.")
            setattr(self, key, value)

    def serialize(self):
        return {field: getattr(self, field) for field in fieldtypes.keys()}

    def unserialize(self, dictionary):
        return cls(**dictionary)
    
    def save(self):
        if not hasattr(self, cls._pk):
            # Invent a new pk
            cls._db
            setattr(self, cls._pk, "")
        cls._db[getattr(self, cls._pk)] = self.serialize()

    
        
    
    cls.__init__ = __init__
    cls.serialize = serialize
    cls.unserialize = unserialize

    
class Database:
    """
    Defines the needs of a JSON-oriented database interface.

    We need to store objects as dictionaries under 
    """
    def 
