from coon.transforms import Transform


class Bootstrapize(Transform):
    """
    Transform class that adds Bootstrap v3 attributes to all elements.
    """
    def transform_component(self, tag, inner, attributes, state=None):
        if tag == "button":
            button_aim = attributes["aim"] if "aim" in attributes else "default"
            del attributes["aim"]
            attributes["type"] = "button"
            attributes["class"] = ["btn", "btn-" + button_aim]
        elif tag == "abstract:alert":
            tag = "div"
            if "aim" not in attributes:
                raise Exception("An alert node must have an aim attribute!")
            alert_aim = attributes["aim"]
            del attributes["aim"]
            attributes["class"] = ["alert", "alert-" + alert_aim]
            attributes["role"] = "alert"
        return tag, inner, attributes, state
