from org.transcrypt.stubs.browser import __pragma__
from coon.components import Module
from coon.pipeline import Pipeline
from coon.tags import Span, Div, Li, Ul, Ol, Input, Button as HTMLButton


__pragma__("kwargs")
__pragma__("opov")


# TODO: Document appropriately all of these classes.

class Glyph(Module):
    """
    Glyph elements taken from Glyphicons.
    """
    def init(self, icon, aria_hidden=True):
        self.icon = icon
        self.aria_hidden = aria_hidden

    def dom_root(self):
        return Span(
            _class=["glyphicon", "glyphicon-" + self.icon],
            aria_hidden=self.aria_hidden,
        )


class Dropdown(Module):
    """
    Toggleable, contextual menu for displaying lists of links.
    """
    def init(self, title, items=[], style="default", dropup=False, size=None, right_align=False):
        self.title = title
        self.items = items
        self.style = style
        self.dropup = dropup
        self.size = size
        self.right_align = right_align

    def _itemkind(self, element):
        kind, item = element
        if kind == "header":
            return Li(inner=item, _class="dropdown-header")
        elif kind == "divider":
            return Li(inner=item, role="separator", _class="divider")
        elif kind == "disabled":
            return Li(inner=item, _class="disabled")
        else:
            raise Exception("Unknown item kind: " + kind)
        
    def dom_root(self):
        return Div(
            _class="dropdown" if not self.dropup else "dropup",
            inner=[
                HTMLButton(
                    _class=((["btn", "btn-" + self.style, "dropdown-toggle"]) +
                            (["btn-" + self.size] if self.size is not None else [])),
                    _type="button",
                    data_toggle="dropdown",
                    aria_haspopup=True,
                    aria_expanded=True,
                    inner=[
                        self.title,
                        Span(_class="caret"),
                    ],
                ),
                Ul(
                    _class=["dropdown-menu"] + ([] if not self.right_align else ["dropdown-menu-right"]),
                    inner=list(map(self._itemkind, self.items)),
                )
            ],
        )


class ButtonGroup(Module):
    "Group a series of buttons together on a single line with the button group."
    def init(self, inner, size=None, vertical=False, justified=False, toolbar=False):
        self.size = size
        self.toolbar = toolbar
        self.inner = inner
        self.vertical = vertical
        self.justified = justified

    def dom_root(self):
        classes = []
        if not self.vertical:
            classes.append("btn-group" if not self.toolbar else "btn-toolbar")
        else:
            classes.append("btn-group-vertical" if not self.toolbar else "btn-toolbar-vertical")
        if self.size is not None:
            classes.append("btn-group-" + self.size)
        if self.justified:
            classes.append("btn-group-justified")
        
        return Div(
            _class=classes,
            role="group" if not self.toolbar else "toolbar",
            inner=self.inner,
        )


class InputGroup(Module, Pipeline):
    """
    Extend form controls by adding text or buttons before, after or on both sizes of any text-based input.
    """
    def init(self, placeholder, input_type="text", id=None, size=None, left_addon=None, right_addon=None):
        self.placeholder = placeholder
        self.left_addon = left_addon
        self.right_addon = right_addon
        self.id = id
        self.size = size
        self.inputel = Input(_class="form-control", _type=input_type, id=self.id, placeholder=self.placeholder)
        # Proxy outgoing messages from inner input
        self(self.inputel)

    def fn(self, out):
        return out

    def dom_root(self):
        return Div(
            _class=["input-group"] + ([] if self.size is None else ["input-group-" + self.size]),
            inner=(
                [] if self.left_addon is None else [Span(_class="input-group-addon", inner=self.left_addon)] +
                [self.inputel] +
                [] if self.right_addon is None else [Span(_class="input-group-addon", inner=self.right_addon)]
            )
        )


class Nav(Module):
    """
    Navigation menus with multiple items.
    
    Items is a list of tuples: (kind, item).
    Kind can be 'active', 'disabled', 'dropdown' or 'default'
    """
    def init(self, items, pills=False, stacked=False, justified=False):
        self.items = items
        self.pills = pills
        self.stacked = stacked
        self.justified = justified

    def _itemkind(self, element):
        kind, item = element
        if kind == "active":
            return Li(role="presentation", _class="active", inner=item)
        elif kind == "disabled":
            return Li(role="presentation", _class="disabled", inner=item)
        elif kind == "dropdown":
            return Li(role="presentation", _class="dropdown", inner=item)
        elif kind == "default":
            return Li(role="presentation", inner=item)
        else:
            raise Exception("Unknown kind " + kind)
        
    def dom_root(self):
        return Ul(
            _class=(["nav"] +
                    (["nav-tabs" if not self.pills else "nav-pills"]) +
                    ([] if not self.stacked else ["nav-stacked"]) +
                    (["nav-justified"] if self.justified else [])),
            inner=list(map(self._itemkind, self.items)),
        )


class Navbar(Module):
    def init(self):
        raise NotImplemented


class Breadcrumb(Module):
    """
    Indicate the current page's location within a navigational hierarchy.

    Accepts the items that are showed one after the other.
    Items is a list of tuples (kind, item), where kind can be "active" or "default".
    """
    def init(self, items):
        self.items = items

    def _itemkind(self, element):
        kind, item = element
        if kind == "active":
            return Li(_class="active", inner=item)
        elif kind == "default":
            return Li(inner=item)
        else:
            raise Exception("Unknown element kind " + kind)
    
    def dom_root(self):
        return Ol(
            _class="breadcrumb",
            inner=list(map(self._itemkind, self.items)),
        )


class Pagination(Module):
    def init(self):
        raise NotImplemented


class Label(Module):
    def init(self, inner, color="default"):
        self.color = color
        self.inner = inner

    def dom_root(self):
        return Span(_class=["label", "label-" + self.color], inner=self.inner)


class Badge(Module):
    def init(self, inner):
        self.inner = inner

    def dom_root(self):
        return Span(_class="badge", inner=self.inner)


class Jumbotron(Module):
    """
    A lightweight, flexible component that can optionally extend the entire viewport to showcase key content on your site.
    """
    def init(self, inner, containerized=False):
        self.inner = inner
        self.containerized = containerized

    def dom_root(self):
        return Div(
            _class="jumbotron",
            inner=(self.inner if not self.containerized else
                   Div(_class="container", inner=self.inner)),
        )


class PageHeader(Module):
    """
    A simple shell for an h1 to appropriately space out and segment sections of content on a page.
    It can utilize the h1's default small element, as well as most other components.
    """
    def init(self, inner):
        self.inner = inner

    def dom_root(self):
        return Div(
            _class="page-header",
            inner=self.inner,
        )


class Row(Module):
    """
    Creates a row of components.
    
    @param lengths: is a dictionary of sizes to number of elements to display for this size.
           e.g. {xs: 6, md: 3}
    """
    def init(self, inner, lengths={"md": 3}):
        self.inner = inner
        self.lengths = lengths

    def dom_root(self):
        return Div(
            _class="row",
            inner=[
                Div(
                    _class=["col-" + size + "-" + str(length) for size, length in self.lengths.items()],
                    inner=element
                )
                for element in self.inner
            ],
        )


class Thumbnails(Module):
    """
    Extend Bootstrap's grid system with the thumbnail component to easily display grids of images, videos, text and more.
    """
    def init(self, inner, caption=None):
        self.inner = inner
        self.caption = caption

    def dom_root(self):
        return Div(
            _class="thumbnail",
            inner=([self.inner] if not isinstance(self.inner, list) else self.inner +
                   [Div(_class="caption", inner=self.caption)] if self.caption is not None else []),
        )


class Alert(Module):
    """
    Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.
    """
    def init(self, inner, color="success", dismissible=False):
        self.color = color
        self.inner = inner
        self.dismissible = dismissible

    def dom_root(self):
        return Div(
            _class=(["alert", "alert-" + self.color] +
                    ([] if not self.dismissible else ["alert-dismissible"])),
            role="alert",
            inner=self.inner
        )


class ProgressBar(Module):
    """
    Provide up-to-date feedback on the progress of a workflow or action with simple yet flexible progress bars.
    """
    def init(self, value, valuemin=0, valuemax=100, valuestart=0, color="default", striped=False, animated=False):
        self.valuemin = valuemin
        self.valuemax = valuemax
        self.valuestart = valuestart
        self.valuenow = value
        self.color = color
        self.striped = striped
        self.animated = animated

    def update_value(self, value):
        self.valuenow = value

    def dom_root(self):
        classes = []
        classes.append("progress-bar")
        classes.append("progress-bar-" + self.color)
        if self.striped:
            classes.append("progress-bar-striped")
        if self.animated:
            classes.append("active")
        return Div(
            _class="progress",
            inner=Div(
                _class=classes,
                role="progressbar",
                aria_valuenow=self.valuenow,
                aria_valuemin=self.valuemin,
                aria_valuemax=self.valuemax,
                style=f"min-width: 2em; width: {self.valuenow}%;",
                inner=self.valuenow,
            ),
        )


class MediaObject(Module):
    def init(self):
        raise NotImplemented


class Panel(Module):
    def init(self, body, heading=None, footer=None, color="default"):
        self.body = body
        self.heading = heading
        self.footer = footer
        self.color = color

    def dom_root(self):
        inner = []
        if self.heading is not None:
            inner.append(Div(_class="panel-heading", inner=self.heading))
        inner.append(Div(_class="panel-body", inner=self.body))
        if self.footer is not None:
            inner.append(Div(_class="panel-footer", inner=self.footer))
        return Div(
            _class=["panel", "panel-" + self.color],
            inner=inner
        )


class Well(Module):
    def init(self, inner, size=None):
        self.inner = inner
        self.size = size

    def dom_root(self):
        return Div(
            _class=["well"] + [] if self.size is None else ["well-" + self.size],
            inner=self.inner,
        )


class Button(Module):
    def init(self, inner, color="default", **kwargs):
        self.inner = inner
        self.color = color
        self.kwargs = kwargs

    def dom_root(self):
        return HTMLButton(
            _type="button",
            _class=["btn", "btn-" + self.color],
            inner=self.inner,
            **self.kwargs,
        )
