from lexicographic_encoding import packb

class Storage:
    storage = None
    def __init__(self, prefix=""):
        self.prefix = prefix

    # TODO: Encode values before saving them to database, so that we can recover tuples and lists
    # ----| and also encode whether we have an item stored in that place or not...
    def __getitem__(self, key):
        return JSON.parse(self.storage.getItem(self.prefix + key))

    def __setitem__(self, key, value):
        self.storage.setItem(self.prefix + key, JSON.stringify(value))

    def __delitem__(self, key):
        self.storage.removeItem(self.prefix + key)

    def items(self):
        for i in range(self.storage.length):
            key = self.storage.key(i)
            value = self.__getitem__(key)
            yield key, value

    def clear(self):
        if self.prefix == "":
            self.storage.clear()
            return
        for key, value in self.items():
            if key.startswith(self.prefix):
                self.__delitem__(key)

    def __len__(self):
        if self.prefix == "":
            return self.storage.length
        count = 0
        for key, value in self.items():
            if key.startswith(self.prefix):
                count += 1
        return count


class LocalStorage(Storage):
    storage = window.localStorage
