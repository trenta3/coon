from org.transcrypt.stubs.browser import __pragma__
from coon.components import Component, OutputValueMixin, InnerSetMixin, InputValueMixin
from coon.pipeline import Pipeline


__pragma__("kwargs")


class A(Component, InnerSetMixin):
    tag = "a"
    

class Abbr(Component):
    tag = "abbr"
    

class Address(Component):
    tag = "address"
    

class Area(Component):
    tag = "area"
    

class Article(Component):
    tag = "article"
    

class Aside(Component):
    tag = "aside"
    

class Audio(Component):
    tag = "audio"
    

class B(Component, InnerSetMixin):
    tag = "b"
    

class Base(Component):
    tag = "base"
    

class Bdi(Component):
    tag = "bdi"
    

class Bdo(Component):
    tag = "bdo"
    

class Blockquote(Component):
    tag = "blockquote"
    

class Body(Component):
    tag = "body"
    

class Br(Component):
    tag = "br"
    

class Button(Component):
    tag = "button"
    

class Canvas(Component):
    tag = "canvas"
    

class Caption(Component):
    tag = "caption"
    

class Cite(Component):
    tag = "cite"
    

class Code(Component):
    tag = "code"
    

class Col(Component):
    tag = "col"
    

class Colgroup(Component):
    tag = "colgroup"
    

class Data(Component):
    tag = "data"
    

class Datalist(Component):
    tag = "datalist"
    

class Dd(Component):
    tag = "dd"
    

class Del(Component):
    tag = "del"
    

class Details(Component):
    tag = "details"
    

class Dfn(Component):
    tag = "dfn"
    

class Dialog(Component):
    tag = "dialog"
    

class Div(Component, Pipeline, InnerSetMixin):
    tag = "div"


class Dl(Component):
    tag = "dl"
    

class Dt(Component):
    tag = "dt"
    

class Em(Component):
    tag = "em"
    

class Embed(Component):
    tag = "embed"
    

class Fieldset(Component):
    tag = "fieldset"
    

class Figcaption(Component):
    tag = "figcaption"
    

class Figure(Component):
    tag = "figure"
    

class Footer(Component):
    tag = "footer"
    

class Form(Component):
    tag = "form"
    

class Head(Component):
    tag = "head"
    

class Header(Component):
    tag = "header"
    

class Hgroup(Component):
    tag = "hgroup"
    

class H1(Component, InnerSetMixin):
    tag = "h1"
    

class H2(Component, InnerSetMixin):
    tag = "h2"
    

class H3(Component, InnerSetMixin):
    tag = "h3"
    

class H4(Component, InnerSetMixin):
    tag = "h4"
    

class H5(Component, InnerSetMixin):
    tag = "h5"
    

class H6(Component, InnerSetMixin):
    tag = "h6"
    

class Hr(Component):
    tag = "hr"
    

class Html(Component):
    tag = "html"
    

class I(Component, InnerSetMixin):
    tag = "i"
    

class Iframe(Component):
    tag = "iframe"
    

class Img(Component):
    tag = "img"
    

class Input(Component, Pipeline, OutputValueMixin, InputValueMixin):
    tag = "input"


class Ins(Component):
    tag = "ins"
    

class Kbd(Component):
    tag = "kbd"
    

class Keygen(Component):
    tag = "keygen"
    

class Label(Component):
    tag = "label"
    

class Legend(Component):
    tag = "legend"
    

class Li(Component):
    tag = "li"
    

class Link(Component):
    tag = "link"
    

class Main(Component):
    tag = "main"
    

class Map(Component):
    tag = "map"
    

class Mark(Component):
    tag = "mark"
    

class Menu(Component):
    tag = "menu"
    

class Menuitem(Component):
    tag = "menuitem"
    

class Meta(Component):
    tag = "meta"
    

class Meter(Component):
    tag = "meter"
    

class Nav(Component):
    tag = "nav"
    

class Noscript(Component):
    tag = "noscript"
    

class Object(Component):
    tag = "object"
    

class Ol(Component):
    tag = "ol"
    

class Optgroup(Component):
    tag = "optgroup"
    

class Option(Component):
    tag = "option"
    

class Output(Component):
    tag = "output"
    

class P(Component):
    tag = "p"
    

class Param(Component):
    tag = "param"
    

class Picture(Component):
    tag = "picture"
    

class Pre(Component):
    tag = "pre"
    

class Progress(Component):
    tag = "progress"
    

class Q(Component):
    tag = "q"
    

class Rp(Component):
    tag = "rp"
    

class Rt(Component):
    tag = "rt"
    

class Ruby(Component):
    tag = "ruby"
    

class S(Component):
    tag = "s"
    

class Samp(Component):
    tag = "samp"
    

class Script(Component):
    tag = "script"
    

class Section(Component):
    tag = "section"
    

class Select(Component):
    tag = "select"
    

class Small(Component, InnerSetMixin):
    tag = "small"
    

class Source(Component):
    tag = "source"
    

class Span(Component, InnerSetMixin):
    tag = "span"
    

class Strong(Component):
    tag = "strong"
    

class Style(Component):
    tag = "style"
    

class Sub(Component):
    tag = "sub"
    

class Summary(Component):
    tag = "summary"
    

class Sup(Component):
    tag = "sup"
    

class Svg(Component):
    tag = "svg"
    

class Table(Component):
    tag = "table"
    

class Tbody(Component):
    tag = "tbody"
    

class Td(Component):
    tag = "td"
    

class Template(Component):
    tag = "template"
    

class Textarea(Component):
    tag = "textarea"
    

class Tfoot(Component):
    tag = "tfoot"
    

class Th(Component):
    tag = "th"
    

class Thead(Component):
    tag = "thead"
    

class Time(Component):
    tag = "time"
    

class Title(Component):
    tag = "title"
    

class Tr(Component):
    tag = "tr"
    

class Track(Component):
    tag = "track"
    

class U(Component):
    tag = "u"
    

class Ul(Component):
    tag = "ul"
    

class Var(Component):
    tag = "var"
    

class Video(Component):
    tag = "video"
    

class Wbr(Component):
    tag = "wbr"
    
