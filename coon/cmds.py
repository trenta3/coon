from inotify.adapters import InotifyTree
from argparse import ArgumentParser
from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer
from functools import partial
from multiprocessing import Process
from os import getcwd
import os
import subprocess


def http_serve(port, directory):
    # Start an HTTP Server from the current path
    handler = partial(SimpleHTTPRequestHandler, directory=directory)
    with TCPServer(("", port), handler) as httpd:
        host, port = httpd.socket.getsockname()[:2]
        url_host = f"[{host}]" if ":" in host else host
        print(f"Serving HTTP on port {port} (http://{url_host}:{port}/)")
        httpd.serve_forever()


def coon():
    parser = ArgumentParser("Coon dev launcher")
    parser.add_argument("--port", type=int, default=8000, help="Port of the webserver to launch")
    parser.add_argument("--directory", type=str, default=getcwd(), help="Directory from which to serve files")
    parser.add_argument("--notify-ignore", type=str, nargs="*", default=["__target__"], help="Things to ignore when inotifying")
    parser.add_argument("--script", type=str, default="main.py", help="Main entry point for transcrypt")
    args = parser.parse_args()

    try:
        phttp = Process(target=http_serve, kwargs=dict(port=args.port, directory=args.directory))
        phttp.start()

        inot = InotifyTree(args.directory)
        valid_flags = ["IN_CREATE", "IN_DELETE", "IN_MODIFY"]

        haschanged = True
        for event in inot.event_gen(yield_nones=True):
            if event is None:
                # End of the single event stream, recompile code!
                if haschanged:
                    print("Recompiling... ", end="\rRecompiling... ")
                    result = subprocess.run(["transcrypt", "-m", args.script], text=True, capture_output=True)
                    if result.returncode != 0:
                        print(f"ERROR\nCompilation returned {result.returncode}. Output follows:\n\n")
                        print(result.stdout)
                        print(result.stderr)
                    else:
                        print("DONE")
                haschanged = False
            else:
                (_, type_names, path, filename) = event
                for ignore in args.notify_ignore:
                    if ignore not in os.path.join(path, filename):
                        if "IN_ISDIR" not in type_names and len(set(type_names).intersection(valid_flags)) >= 1:
                            print(f"{type_names}: {path}/{filename}")
                            haschanged = True
                        
    except KeyboardInterrupt as e:
        print("Terminating...")
    finally:
        phttp.terminate()
