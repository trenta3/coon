from org.transcrypt.stubs.browser import __pragma__


__pragma__("kwargs")
__pragma__("opov")


def deepcopy(obj):
    immutable = [bool, str, int, float]
    if obj == None:
        return None
    for T in immutable:
        if isinstance(obj, T):
            return obj
    # Else is mutable
    if isinstance(obj, list):
        return [deepcopy(el) for el in obj]
    elif isinstance(obj, dict):
        return {deepcopy(key): deepcopy(value) for key, value in obj.items()}
    else:
        # TODO: Implement classes and closures...
        return obj


def pp(struct):
    if struct is None:
        return "None"
    for T in [bool, str, int, float]:
        if isinstance(struct, T):
            return str(struct)
    if isinstance(struct, list):
        return "[" + ", ".join(map(pp, struct)) + "]"
    if isinstance(struct, tuple):
        return "(" + ", ".join(map(pp, struct)) + ")"
    if isinstance(struct, dict):
        return "{" + ", ".join((pp(k) + ": " + pp(v) for k, v in struct.items())) + "}"
    if hasattr(struct, "__class__"):
        return "<Instance of " + struct.__class__.__name__ + ">"
    return str(struct)


joindicts = lambda d1, d2: {k: v for k, v in d1.items() + d2.items()}


class partial:
    def __init__(self, func, *args, **kwargs):
        if not callable(func):
            raise TypeError("The first argument must be callable")

        self.func = func
        self.args = args
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs):
        keywords = joindicts(self.kwargs, kwargs)
        return self.func(*self.args, *args, **keywords)
