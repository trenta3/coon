from org.transcrypt.stubs.browser import __pragma__
from coon.pipeline import Pipeline
import random, logging


__pragma__("kwargs")
__pragma__("opov")


class Component:
    tag = None
    
    def __init__(self, inner=None, **attributes):
        self.inner = inner
        self.attributes = attributes
        self.element = None

    def bind(self, element):
        "Bind the component to an element of the DOM"
        self.element = element

    def update(self):
        "Update the real DOM component"
        if self.element is not None:
            newelement = self.render()
            self.element.replaceWith(newelement)
            self.element = newelement
        else:
            raise Exception(f"None element found in {self}")
    
    def render(self):
        """
        Creates a virtual DOM element and then replaces the current one with the new one.
        """
        element = document.createElement(self.tag if self.tag is not None else "x-undef")
        # First updates all attributes of the element
        for aname, aval in self.attributes.items():
            if aval is None:
                continue
            if isinstance(aval, list):
                aval = " ".join(aval)
            if aname.startswith("_"):
                aname = aname[1:]
            aname = aname.replace("_", "-")
            # Add event listeners to standard events
            if aname.startswith("on"):
                element.addEventListener(aname[2:], aval)
            else:
                if callable(aval):
                    aval = aval()
                element.setAttribute(aname, aval)
        if self.inner is not None:
            # Then add all the inner things, by referencing inner item element if present!
            for el in self.inner if isinstance(self.inner, list) else [self.inner]:
                if callable(el) and not hasattr(el, "__call__"):
                    el = el()
                if isinstance(el, str):
                    element.append(el)
                elif isinstance(el, Component) and el.element is not None:
                    # If the inner element is already bound just update it
                    el.update()
                    element.append(el.element)
                elif isinstance(el, Component):
                    custom = document.createElement("x-temp")
                    element.append(custom)
                    el.bind(custom)
                    el.update()
                else:
                    raise Exception(f"Unknwon element asked to render: {el=} in {self.__class__.__name__=}")
        return element


class Module(Component):
    "Module class for components which do not take inner inputs, but can have state"
    def __init__(self, *args, **kwargs):
        self.transformations = []
        super().__init__()
        if hasattr(self, "init"):
            self.init(*args, **kwargs)

    def add_transform(self, transform):
        self.transformations.append(transform)
            
    def dom_root(self):
        raise NotImplemented
            
    def render(self):
        element = self.dom_root()
        for transform in self.transformations:
            element = transform(element)
        return element.render()


class OutputValueMixin(Component, Pipeline):
    "For components that return the self.element.value whenever it changes"
    def __init__(self, *args, **kwargs):
        changed = lambda: self.output_changed(self.element.value)
        super().__init__(
            self,
            *args,
            oninput=changed,
            onchange=changed,
            **kwargs,
        )


class InputValueMixin(Component, Pipeline):
    "For components that set their element.value as pipeline output"
    def fn(self, value):
        self.element.value = value
        self.update()


class InnerSetMixin(Component, Pipeline):
    "For components that set their inner property as the pipeline output"
    def fn(self, inner):
        self.inner = inner
        self.update()
