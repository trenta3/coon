#!/usr/bin/env python3

from distutils.core import setup

setup(
    name="coon",
    version="0.1.0",
    description="Code Web UI in Python using Transcrypt",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author="Dario Balboni",
    author_email="dario.balboni.96+coon@gmail.com",
    url="https://gitlab.com/trenta3/coon",
    py_modules=["coon"],
    license="GPL-3.0",
    install_requires=["transcrypt", "inotify", "PyContracts3"],
    entry_points=dict(
        console_scripts=[
            "coon=coon.cmds:coon",
        ],
    ),
)
