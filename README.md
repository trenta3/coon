# Coon - Web UI programming in Python

## Quick Start
To run one of the examples available under `examples/` folder:
```bash
cd examples/counter
transcrypt -m main.py
python3 -m http.server
```
