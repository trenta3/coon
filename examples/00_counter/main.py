import logging
from coon.components import Module
from coon.tags import Div, Button


__pragma__("kwargs")


class Counter(Module):
    def init(self, start=0):
        self.counter = start
        self.main = Div(inner=[
            lambda: f"Count: {self.counter}",
            Button(
                inner="Increment counter",
                onclick=self.increment_counter,
            ),
            Button(
                inner="Decrement counter",
                onclick=self.decrement_counter,
            ),
        ])

    def decrement_counter(self):
        self.counter -= 1
        self.update()
        
    def increment_counter(self):
        self.counter += 1
        self.update()

    def render(self):
        return self.main.render()


window.title = "CounterApp"
maindiv = document.createElement("div")
document.body.append(maindiv)
counter = Counter(start=10)
counter.bind(maindiv)
counter.update()
