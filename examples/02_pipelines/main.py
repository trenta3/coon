from org.transcrypt.stubs.browser import __pragma__
import logging
from coon.components import Module
from coon.bootstrap.components import InputGroup, PageHeader, Row
from coon.tags import Main, Input, Div, H1, Small, B, P
from coon.pipeline import P, Pipeline


__pragma__("kwargs")
__pragma__("opov")


class NameApp(Module):
    def init(self):
        self.full_name = None
        self.name_input = InputGroup(placeholder="Your Name", input_type="text", left_addon="Name")
        self.surname_input = InputGroup(placeholder="Your Surname", input_type="text", left_addon="Surname")
        self.full_output = Div()
        self.main = Main(
            style="margin: 20px auto; width: 80%;",
            inner=[
                PageHeader(inner=H1(["NameApp ", Small("Write your name and you will be greeted!")])),
                Row([self.name_input, self.surname_input], lengths={"lg": 6}),
                self.full_output,
            ],
        )

        # Setup of the pipeline elements
        pipe = P(lambda name, surname: f"{name} {surname}")
        pipe(self.name_input, self.surname_input)
        self.full_output(pipe)

    def dom_root(self):
        return self.main


window.title = "NameApp"
maindiv = document.createElement("div")
document.body.append(maindiv)
app = NameApp()
app.bind(maindiv)
app.update()
 
