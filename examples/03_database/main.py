from org.transcrypt.stubs.browser import __pragma__
import logging
from coon.components import Module
from coon.bootstrap.components import InputGroup, PageHeader, Row
from coon.tags import Main, Input, Div, H1, Small, B, P
from coon.pipeline import P, Pipeline
from coon.dataclasses import dataclass, String, Boolean, List
from coon.storage import LocalStorage


__pragma__("kwargs")
__pragma__("opov")


@dataclass
class Post:
    _pk = title
    _db = LocalStorage("post")
    published = Boolean(default=False)
    title = String()
    body = String()

    # Each dataclass or class instance can have actions to be executed on it!
    def save(self) -> None:
        "Creates or modifies the current instance"
        pass

    # Then in a transaction we can say:
    # Post pk=1 publish *args **kwargs
    # Post _ create **kwargs
    # Post _ search **kwargs ==> List[Post]
    # If we remove the requirement for a transaction we can also retrieve data from remote and act on it.
    # Otherwise we must have a way to express possible computations on data and send them along the transaction.
    def publish(self):
        pass
    
    @classmethod
    def update(cls, **kwargs):
        pass
    
    @classmethod
    def create(cls, **kwargs) -> Post:
        "Creates a new object of said class with the passed attributes"
        pass

    @classmethod
    def get(cls, pk) -> Post:
        "Retrieves a single dataclass instance given the primary key"
        pass

    @classmethod
    def search(cls, **kwargs):
        "Returns an iterable of posts satifying the search criteria"
        pass


class App(Module):
    def init(self):
        self.full_name = None
        self.name_input = InputGroup(placeholder="Your Name", input_type="text", left_addon="Name")
        self.surname_input = InputGroup(placeholder="Your Surname", input_type="text", left_addon="Surname")
        self.full_output = Div()
        self.main = Main(
            style="margin: 20px auto; width: 80%;",
            inner=[
                PageHeader(inner=H1(["NameApp ", Small("Write your name and you will be greeted!")])),
                Row([self.name_input, self.surname_input], lengths={"lg": 6}),
                self.full_output,
            ],
        )

        # Setup of the pipeline elements
        pipe = P(lambda name, surname: f"{name} {surname}")
        pipe(self.name_input, self.surname_input)
        self.full_output(pipe)

    def dom_root(self):
        return self.main


window.title = "Posting App"
maindiv = document.createElement("div")
document.body.append(maindiv)
app = App()
app.bind(maindiv)
app.update()
 
