import logging
from coon.components import Module
from coon.tags import Span, Main
from coon.bootstrap.components import ProgressBar, ButtonGroup, Panel, Button


__pragma__("kwargs")


class Counter(Module):
    def init(self, start=0):
        self.counter = start
        self.main = Main(
            style="margin: 20px auto; width: 80%;",
            inner=Panel(
                heading="A simple counter application",
                body=[
                    lambda: ProgressBar(self.counter, color="primary"),
                    ButtonGroup(
                        justified=True,
                        inner=[
                            ButtonGroup(
                                Button("Increment counter", color="primary", onclick=self.increment_counter)
                            ),
                            ButtonGroup(
                                Button("Decrement counter", color="warning", onclick=self.decrement_counter)
                            ),
                        ],
                    )
                ],
            ),
        )

    def decrement_counter(self):
        self.counter -= 1
        if self.counter < 0:
            self.counter = 100
        self.update()
        
    def increment_counter(self):
        self.counter += 1
        if self.counter > 100:
            self.counter = 0
        self.update()

    def dom_root(self):
        return self.main


window.title = "CounterApp"
maindiv = document.createElement("div")
document.body.append(maindiv)
counter = Counter(start=10)
counter.bind(maindiv)
counter.update()
